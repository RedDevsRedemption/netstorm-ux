import { Component, OnInit } from '@angular/core';
import { remote } from 'electron';

@Component({
  selector: 'app-window-frame',
  templateUrl: './window-frame.component.html',
  styleUrls: ['./window-frame.component.scss']
})
export class WindowFrameComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  closeWindow() : void {
    let w = remote.getCurrentWindow();
    w.close();
  }

  minimizeWindow(){
    let w = remote.getCurrentWindow();
    w.minimize();
  }

  maximizeWindow(){
    let w = remote.getCurrentWindow();
    if(w.isMaximized())
      w.restore();
    else
      w.maximize();
  }

}
